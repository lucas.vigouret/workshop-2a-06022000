<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="../style/style_admin.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <title>Admin</title>
</head>
<body>
	<header>
		<img class="logo" src="../images/logo.png">
	</header>

<?php

include '../include/config.inc.php';
include '../include/fonctions.php';

// Appel du tableau $_SESSION



// Cas d'un utilisateur ayant voulu afficher une page privée sans connexion

if (isset($_GET["pirate"]))
{
	echo("<p>Bien essayé mais il faut se connecter... :-)</p>");
}

// Déconnexion

if (isset($_GET["logout"]))
{
	unset($_SESSION["admin"]);
	
	// On peut également utiliser session_destroy()
	
	echo("<p>Vous avez bien été déconnecté</p>");
}

// Traitement

if (!empty($_POST))
{
	// Récupération des données de formulaires
	
	$mail = change($_POST["mail"]);
	$password = change($_POST["password"]);
	
	// Compte valable

	$sql = "select * from admin where admin_mail='".$mail."' AND admin_mdp='".$password."'";

    $query = mysqli_query($lien,$sql);

    $result = mysqli_fetch_assoc($query);

	if ($result["admin_mail"] == "")
	{
		echo("<p>Erreur d'authentification !</p>");
	}
	
	// L'utilisateur existe
	
	else
	{
		// Création d'une variable session
		
		$_SESSION["admin"] = $result["id_admin"];
		
		// Redirection vers la page d'accueil privée
		
		header("location:../admin/index_admin.php");
	}
}

// Formulaire HTML en méthode POST qui renvoie vers la même page

?>
<div class="connexion">
	<div class="titleAdmin">
		<h1>Administration</h1>
	</div>
	<div class="containerConnexion">
		<form method="post" action="index.php">
		<div class="mailConnexion">E-mail <span class="asterix">*</span> <br> <input class="insert" type="email" name="mail" required /></div>
		<div class="mdpConnexion">Mot de passe <span class="asterix">*</span> <br> <input class="insert" type="password" name="password" required /></div>
		<input class="btnConnexion" type="submit" value="Se connecter" />
		</form>
	</div>

</div>


</body>
</html>
